#+TITLE: Rust Exercises

* Table of Contents :TOC:
- [[#description][Description]]

* Description
These are exercises completed while following along with [[https://doc.rust-lang.org/stable/book][The Rust Book]].

Feel free to look inside if you want to 😏
